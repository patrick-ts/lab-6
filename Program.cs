﻿using System.Diagnostics;

var random = new Random();

static void Exec(int PID, double time)
{
    Console.WriteLine($"Process {PID} is running...");
    Thread.Sleep((int)(time * 1000));
    Console.WriteLine($"Process {PID} was finished.");
}

void SequentialProcessing(int n)
{
    var watch = Stopwatch.StartNew();

    for (int i = 0; i < n; i++)
    {
        int PID = random.Next(1000, 5000);
        int time = random.Next(4, 12);

        Exec(PID, time);
    }

    watch.Stop();
    Console.WriteLine($"Processes time: {Math.Round(watch.Elapsed.TotalSeconds)}s");
}

void AsyncProcessing(int n)
{
    var watch = Stopwatch.StartNew();
    var processes = new List<Thread>();

    for (int i = 0; i < n; i++)
    {
        int PID = random.Next(1000, 5000);
        int time = random.Next(4, 12);

        var process = new Thread(() => Exec(PID, time));
        process.Start();
        processes.Add(process);
    }

    foreach (var process in processes)
    {
        process.Join();
    }

    watch.Stop();
    Console.WriteLine($"Processes time: {Math.Round(watch.Elapsed.TotalSeconds)}s");
}

var n = random.Next(3, 7);

SequentialProcessing(n);

AsyncProcessing(n);
